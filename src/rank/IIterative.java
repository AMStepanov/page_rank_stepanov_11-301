package rank;

import model.sparsematrix.SparseMatrix;

public interface IIterative {

    double [] calculate (SparseMatrix sparseMatrix);

    long getTime();

}
