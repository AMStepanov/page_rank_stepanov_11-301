package main;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

import crawler.PageCrawler;
import model.Matrix;
import model.Result;
import model.Webpage;
import rank.IIterative;
import rank.Iterative;
import rank.MultiIterative;


public class Program {
	
	//site URL
	public final static String START_PAGE = "http://kpfu.ru/";
	
	
	//MAIN METHOD! Start here!
	public static void main(String[] args) throws Exception {
		
		
		//For results
		Result webpages = Result.INSTANCE;
        PrintWriter printWriter = new PrintWriter("rank ex3 ex4.txt");
        
        //First page
        webpages.add(new Webpage(START_PAGE));
        
        
        PageCrawler crawler = new PageCrawler();
        Matrix matrix = new Matrix();
        //1
        while (webpages.hasNext())
            try {
                Webpage page = webpages.next();
                matrix.extractLinks(webpages, crawler, page);
                page.setCrawled(true);
            } catch (Exception e) {
                e.printStackTrace();
            }

        //if simple matrix then true
        //if sparse matrix then false
        boolean b = true;

        // 3 (iterative)
        calculate(webpages, matrix, new Iterative(), !b , printWriter);

        printWriter.println();
        printWriter.println();
        printWriter.println();

        // 4 (MultiIterative)
        calculate(webpages, matrix, new MultiIterative(), !b , printWriter);
       
        printWriter.close();

	
	}
	
	private static void calculate(Result webpages,  Matrix matrix, IIterative iIterative, boolean b, PrintWriter printWriter) throws FileNotFoundException{


        matrix.prepareResult(webpages,b);
        double[] rank = iIterative.calculate(matrix.getSparseMatrix());

        Map<String, Double> result = new HashMap<>();
        IntStream.range(0, rank.length)
                .forEach(r -> result.put(webpages.getWebpages().get(r).getUrl(), rank[r]));
        result.entrySet().stream()
                .sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue()))
                .forEach(e -> {
                    printWriter.print(e.getKey() + "\t");
                    printWriter.println(e.getValue());
                });
        printWriter.println("Runtime: " + iIterative.getTime() + " ms");
    }
	
	

}

